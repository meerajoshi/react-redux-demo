## Description
This demo is to demonstrate React + Redux with persistent state. This includes 'User Profile detail' forms in different pages. All the form's field value are stored in local storage until user manually clear the form or submit the form details. Value of the form fiels are being printed in log when the user save the form, You can see in browser console.
This demo uses following modules with react :

| Module | Description |
| --- | --- |
| redux | Predictable state container for JavaScript apps |
| react-redux | Designed to work with React's component model for redux |
| redux-persist | Provides a consistent and structured way to persist state |
| redux-form | The best way to manage your form state in Redux |
| redux-logger | A production Redux logging tool |
| bootstrap | The world's most popular framework for building responsive, mobile-first front-end web development. |
| react-bootstrap | Designed to work with React's component model for bootstrap |

## Install Node.js if not already installed
[Follow the link for installation guide](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions-enterprise-linux-fedora-and-snap-packages)

## Navigate to the extracted directory 'react-redux-demo'
`cd react-redux-demo/`

## Install required node modules to start the project
`npm install`

## Start the server
`npm start`

Runs the app in the development mode. Open [localhost:3000](http://localhost:3000) to view it in the browser.