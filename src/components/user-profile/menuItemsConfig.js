import PersonalDetailForm from './forms/PersonalDetailForm';
import AddressDetailForm from './forms/AddressDetailForm';
import EducationDetailForm from './forms/EducationDetailForm';
import { reset } from 'redux-form';

const menuItemsConfig = {
  menuItems : [
    {menuLink:'/personal-detail', menuTitle:'Personal Detail', component:PersonalDetailForm},
    {menuLink:'/address-detail', menuTitle:'Address Detail', component:AddressDetailForm},
    {menuLink:'/education-detail', menuTitle:'Eduction Detail', component:EducationDetailForm},
  ],
  activeMenuItemClassName : 'active',
  menuItemClassName : ['list-group-item', 'list-group-item-action', 'text-center'],
}

const getAdjoinFormsLink = (componentName) => {
  let adjoinFormsLink = {
    next : undefined,
    prev : undefined
  };
  menuItemsConfig.menuItems.forEach(function(menuItem, index){
    if(componentName === menuItem.component.defaultProps.form){
      adjoinFormsLink.prev = (menuItemsConfig.menuItems[index-1])?menuItemsConfig.menuItems[index-1].menuLink:undefined;
      adjoinFormsLink.next = (menuItemsConfig.menuItems[index+1])?menuItemsConfig.menuItems[index+1].menuLink:undefined;
    }
  });
  return adjoinFormsLink;
};

const resetUserProfileForms = (dispatch) => {
  menuItemsConfig.menuItems.forEach(function(menuItem){
    dispatch(reset(menuItem.component.defaultProps.form));
  });
  return true;
}

export { menuItemsConfig, getAdjoinFormsLink, resetUserProfileForms }